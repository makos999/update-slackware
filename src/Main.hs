{-# LANGUAGE OverloadedStrings #-}

-- todo: get OS version from '/etc/slackware-version

module Main where

import           Data.Word
import           Data.Attoparsec.ByteString.Char8  
import qualified Data.ByteString.Char8 as BC
import qualified Data.ByteString.Lazy as L
import           Data.List (sortBy)
import qualified Data.List.Split as S
import qualified Data.Set as S
import qualified Data.Text as T
import           Data.Text.Metrics (levenshtein)
import           Prelude hiding (takeWhile)
import           Network.HTTP.Conduit
import           System.Directory (listDirectory)

import           SlackwareVer

import qualified Control.Exception as E
import Network.HTTP.Simple

data IP = IP Word8 Word8 Word8 Word8  deriving Show
data MyDate = MyDate Integer Word8 Word8 
data Package = Package BC.ByteString deriving (Show, Eq, Ord)


funwrap (Nothing) = return ()
funwrap (Just (l,r,_)) = do
  let lx = parseOnly f (BC.pack l)
  let rx = parseOnly f (BC.pack r)
  let x = pc lx rx
  print $ pc lx rx
  return ()
  where
    f :: Parser String
    f = manyTill anyChar (string "-")
    pc :: Either String String -> Either String String -> (String, Integer)
    pc (Right r1) (Right r2) = distance r1 r2
    pc (Right _) (Left x) = (x,9)
    pc (Left _) (Right x) = (x,9)
    pc  (Left _)  (Left x) = (x,9)

listingOfUpdates :: IO [Maybe (String,String,Integer)]
listingOfUpdates = getCandidates return

getCandidates :: (Maybe (String,String,Integer) -> IO a) -> IO [a]
getCandidates actor = do
  lps <- fetchLocalPackageSet
  rps <- fetchRemotePackageSet
  mapM actor $ buildUpdatelist rps lps
  where
    buildUpdatelist r l = map (findUpdates r) (unwrapToList l)

unwrapToList = map (\(Package x) -> BC.unpack x) . S.toList


--printIfUpdate :: Show t => Maybe t -> IO ()
printIfUpdate = do
  x <- listingOfUpdates
  mapM_ print (filter (/= Nothing) x )

--foo' :: Maybe (String,String,Integer) -> IO ()
downloadIfUpdate :: Maybe (String,String,Integer) -> IO ()
downloadIfUpdate x = case x of
  Nothing -> return ()
  Just y -> do
    print $ "Downloading ... " ++ (snd' y)
    eresponse <- E.try $ simpleHttp ("http://ftp.slackware.com/pub/slackware/slackware64-14.1/patches/packages/" ++ snd' y ++ ".txz")
    case eresponse of
      Left e -> print (e::HttpException)
      Right r -> L.writeFile ("/home/makos/mem/new-updates/" ++ (snd' y) ++ ".txz")  r
  where
    snd' (_,x,_) = x
{-
Just ("samba-4.2.14-x86_64-1_slack14.1","samba-4.4.12-x86_64-1_slack14.1",2)
Just ("udisks-1.0.5-x86_64-1_slack14.1","udisks2-2.1.3-x86_64-1_slack14.1",4)
Just ("udisks2-2.1.3-x86_64-1_slack14.1","udisks-1.0.5-x86_64-1_slack14.1",4)
-}
findUpdates' :: S.Set Package -> String -> Maybe (String,String,Integer)
findUpdates' r lPkg = wrap . h . sortBy rank . filter same  $ map (distance lPkg) r''
  where
    r'' = S.toList $ S.map (\(Package x) -> BC.unpack x) r -- fasz
    rank x y = compare (snd x) (snd y)
    same rPkg = ((compareAppInit (fst rPkg) lPkg) == 0)
                && (snd rPkg /= 0)
    compareAppInit s1 s2 = snd $ distance (check s1) (check s2)
    check = head . S.splitOn "-" 
    h [] = []
    h (x:xs) = [x]
    wrap [] = Nothing
    wrap (x:xs)  = Just $ plan x
    plan (r,d) = (lPkg,r,d)

--______________________________________________________________________

findUpdates :: S.Set Package -> String -> Maybe (String,String,Integer)
findUpdates r lPkg = wrap . h . sortBy rank . filter same $ map (distance lPkg) r''
  where
    r'' = S.toList $ S.map (\(Package x) -> BC.unpack x) r -- fasz
    rank x y = compare (snd x) (snd y)
    same x = snd x /= 0 && snd x <= 2
    h [] = []
    h (x:xs) = [x]
    wrap [] = Nothing
    wrap (x:xs)  = Just $ plan x
    plan (r,d) = (lPkg,r,d)

distance :: String -> String -> (String, Integer)
distance local remote = (remote, sum $ f local remote )
   where
     f x y = map (\x->distance' x) $ zip (pkgSplit x) (pkgSplit y)
     distance' x = toInteger $ levenshtein (T.pack $ fst x) (T.pack $ snd x)
     pkgSplit = S.splitOn "-"

parsePkgName :: Parser String
parsePkgName = manyTill anyChar (string "i586")

procLocalPkgSet x = do
  case (parseOnly parsePkgName $ BC.pack x) of
    Right r -> r
    Left r  -> "null"

fetchLocalPackageSet :: IO (S.Set Package)
fetchLocalPackageSet = do
  l <- listDirectory "/var/log/packages/"
  return (S.fromList . map (Package . BC.pack) $ l)
  where quux x = Prelude.take ((length x) -14) x -- - (length pfx)
        pfx = "-x86_64-1_slack14.1" :: String

fetchRemotePackageSet :: IO (S.Set Package)
fetchRemotePackageSet = do
  patchFile <- fetchPatchFile >>= simpleHttp
  case (parseOnly parseFile $ (BC.concat . L.toChunks) $ patchFile) of
    Right r -> return $ S.fromList r
    Left m -> foo m --todo

fetchPatchFile = do
  ver <- slackVersion
  case ver of
    Nothing -> return "null" -- $ "Unable to determine local Slackware version. Check " ++ slackVersionFile ++ "!"
    Just r -> return $ "http://ftp.slackware.com/pub/slackware/slackware64-" ++ r ++ "/patches/PACKAGES.TXT"
    
foo errMsg = do
  print errMsg
  return (S.fromList [])

parseFile :: Parser [Package]
parseFile = do
  many1 $ parsePackage <* endOfLine

--    string "<!--" *> manyTill anyChar (string "-->")
parsePackage :: Parser Package
parsePackage = do
  a <- manyTill anyChar (string "PACKAGE NAME:  ")
  b <- takeTill (\x -> (x == '\n'))
  return $ Package (BC.take ((BC.length b) - 4) b)
  where
    pfx = "-x86_64-1_slack14.1.txz" -- todo remove this line maybe
  
main :: IO [()]
main = do
  slackVersion >>= print
  listingOfUpdates >>= mapM downloadIfUpdate
