module SlackwareVer
 ( slackVersion
 , slackVersionFile)
where

import Data.Attoparsec.ByteString.Char8
import qualified Data.ByteString as B
import qualified Data.ByteString.Char8 as BC

type Version = String
type Distribution = String

slackVersionFile :: String
slackVersionFile = "/etc/slackware-version"

getDistVersion :: Parser (Distribution,Version)
getDistVersion = do
  d <- many1 letter_ascii
  x <- space
  v <- takeByteString
  return ( d
         , BC.unpack . BC.filter (inClass "0123456789.") $ v)

slackVersion :: IO (Maybe Version)
slackVersion = do
  verf <- BC.readFile slackVersionFile
  case (parseOnly getDistVersion verf) of
    Left r -> return Nothing
    Right r -> return $ checkDistro r

-- the distribution must be Slackware, otherwise running the application is not valid at all. 
checkDistro ("Slackware", v) = Just v
checkDistro (_,_) = Nothing
